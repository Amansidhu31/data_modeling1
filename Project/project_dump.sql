-- MariaDB dump 10.18  Distrib 10.4.17-MariaDB, for Win64 (AMD64)
--
-- Host: localhost    Database: project
-- ------------------------------------------------------
-- Server version	10.4.17-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `customers`
--

DROP TABLE IF EXISTS `customers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `customers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `f_name` varchar(255) DEFAULT NULL,
  `l_name` varchar(255) NOT NULL,
  `register_at` datetime NOT NULL,
  `email` varchar(255) DEFAULT NULL,
  `cus_password` float NOT NULL DEFAULT 0,
  `admin` tinyint(1) DEFAULT NULL,
  `country` varchar(255) NOT NULL,
  `city` varchar(255) DEFAULT NULL,
  `phone` varchar(255) DEFAULT NULL,
  `address` varchar(255) NOT NULL,
  `gender` enum('Femal','Male') DEFAULT NULL,
  `createdAt` datetime NOT NULL DEFAULT current_timestamp(),
  `updatedAt` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `customers`
--

LOCK TABLES `customers` WRITE;
/*!40000 ALTER TABLE `customers` DISABLE KEYS */;
INSERT INTO `customers` VALUES (1,'Dulea','Keirr','0000-00-00 00:00:00','dk@gmail.com',234.5,1,'Canada','Winnipeg','2046089421','810 chancellor','Male','2021-04-15 15:02:01',NULL),(2,'Lockwood','Gary','0000-00-00 00:00:00','lg@gmail.com',1234.1,1,'Canada','Winnipeg','2046884431','944 pemibina hwy','Male','2021-04-15 15:02:01',NULL),(3,'Rain','Douglas','2000-07-09 00:00:00','rg@hotmail.com',582.6,0,'Canada','Toronto','2046089421','810 Guelph st','','2021-04-15 15:07:11',NULL),(4,'Fisher','Carrie','0000-00-00 00:00:00','fc@yahoo.com',686.6,0,'India','Faridkot','431658421','810-944 pemibina hwy','','2021-04-15 15:16:17',NULL),(5,'Hammil','Mark','0000-00-00 00:00:00','hammk@outlook.com',8012.3,0,'USA','new York','604897341','1213 st.marry rd','','2021-04-15 15:16:17',NULL);
/*!40000 ALTER TABLE `customers` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `details`
--

DROP TABLE IF EXISTS `details`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `details` (
  `order_id` int(11) NOT NULL,
  `menu_id` int(11) NOT NULL,
  PRIMARY KEY (`order_id`,`menu_id`),
  KEY `menu_id` (`menu_id`),
  CONSTRAINT `details_ibfk_1` FOREIGN KEY (`menu_id`) REFERENCES `menu` (`menu_id`),
  CONSTRAINT `details_ibfk_2` FOREIGN KEY (`order_id`) REFERENCES `orders` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `details`
--

LOCK TABLES `details` WRITE;
/*!40000 ALTER TABLE `details` DISABLE KEYS */;
INSERT INTO `details` VALUES (1,1),(6,2),(7,3),(8,4),(9,5);
/*!40000 ALTER TABLE `details` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `menu`
--

DROP TABLE IF EXISTS `menu`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `menu` (
  `menu_id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) DEFAULT NULL,
  `type` varchar(255) NOT NULL,
  `description` text DEFAULT NULL,
  `Veg` enum('Y','N') DEFAULT NULL,
  `price` decimal(9,4) NOT NULL,
  `calories` varchar(255) DEFAULT NULL,
  `cooking` tinyint(1) NOT NULL DEFAULT 0,
  `image` longblob DEFAULT NULL,
  `vendor` tinyint(1) NOT NULL DEFAULT 0,
  `Active` tinyint(1) DEFAULT NULL,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime DEFAULT NULL,
  PRIMARY KEY (`menu_id`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `menu`
--

LOCK TABLES `menu` WRITE;
/*!40000 ALTER TABLE `menu` DISABLE KEYS */;
INSERT INTO `menu` VALUES (1,'Chicken Dumplings','Lunch','Bite-size dumplings filled with perfect blend of tender chicken mince','N',9.9500,'300',1,'',0,1,'0000-00-00 00:00:00',NULL),(2,'Beef Dumplings','Lunch','Seasoned ground or minced beef, placed on bean curd skins and steamed','N',10.9500,'400',1,'',1,1,'0000-00-00 00:00:00',NULL),(3,'Pawn Dumplings','Lunch','Dive into thses bite-sized dumplings with pawn filling','N',8.9500,'300',1,'',0,1,'0000-00-00 00:00:00',NULL),(4,'Mixed-Veg Dumplings','Lunch','Served in the traditional bamboo steamer, these steamed dumplings are every veg-lover dream','Y',8.9500,'300',0,'',0,1,'0000-00-00 00:00:00',NULL),(5,'Kurkra Dumplings','Lunch','Bite-size dumplings filled with perfect blend of kurkre and onions','Y',9.9500,'250',1,'',0,1,'0000-00-00 00:00:00',NULL),(6,'Curried Cuttlefish','Lunch','Squid or cuttlefish is marinated in a curry sauce and steamed','N',9.9500,'250',1,'',0,1,'0000-00-00 00:00:00',NULL),(7,'Veg-Bryani','Lunch','Veg-bryiani with delicious vegetables chunks and cheese','Y',9.9500,'350',1,'',1,1,'0000-00-00 00:00:00',NULL),(8,'Veg-Noodles','Dinner','seasoned with oyster sauce, soy sauce, and sugar.','N',9.9500,'350',1,'',0,1,'0000-00-00 00:00:00',NULL),(9,'Egg Custard','Desert','A steamed custard that may include meat or seafood','N',5.9500,'300',1,'',1,1,'0000-00-00 00:00:00',NULL),(10,'Mango Pudding','Desert','A light, refreshing dim sum dessert made with sweet fresh mango fruit.','N',5.9500,'300',0,'',1,0,'0000-00-00 00:00:00',NULL),(11,'Sesame Seed Balls','Starters','Balls of glutinous rice flour and brown sugar are filled with sweet red bean paste, rolled in sesame seeds, and deep-fried','Y',7.9500,'300',1,'',0,1,'0000-00-00 00:00:00',NULL),(12,'Shrimp Toast','Starter','A shrimp paste mixture is spread on toast points and deep-fried.','N',7.9500,'350',1,'',0,1,'0000-00-00 00:00:00',NULL),(13,'Spring Rolls','Starter','A classic spring roll filling consists of dried mushrooms, and shredded meat, and carrot or bamboo shoots.','N',8.9500,'170',0,'',1,1,'0000-00-00 00:00:00',NULL),(14,'Berrys Cheesecake','Desert','6oz. and served with berrys ','Y',5.9500,'150',1,'',0,1,'0000-00-00 00:00:00',NULL),(15,'Chocolate Brownie','Desert','Flavoured with Ice cream ','Y',5.9500,'300',1,'',0,1,'0000-00-00 00:00:00',NULL),(16,'Sizzling White Chocolate Cake','Desert','6oz. and served with berrys ','Y',6.9500,'200',1,'',0,1,'0000-00-00 00:00:00',NULL),(17,'Stuffed Green Pepper','diner','Squares of green bell pepper are covered with shrimp paste and fried ','N',6.9500,'350',1,'',1,0,'0000-00-00 00:00:00',NULL),(18,'Stuffed Eggplant','Diner','Purple eggplant is stuffed with a shrimp paste fried and then topped with sauce.','N',9.9500,'300',1,'',0,1,'0000-00-00 00:00:00',NULL),(19,'Roast Pork','Dinner','The filling used in roast pork buns','N',10.9500,'400',1,'',1,1,'0000-00-00 00:00:00',NULL),(20,'Chicken Pasta','Dinner','Pasta made in delicious Alferedo Sauce ','Y',8.7500,'300',1,'',0,0,'0000-00-00 00:00:00',NULL);
/*!40000 ALTER TABLE `menu` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `orders`
--

DROP TABLE IF EXISTS `orders`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `orders` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `customer_id` int(11) NOT NULL,
  `order_datetime` datetime NOT NULL,
  `order_status` varchar(255) NOT NULL,
  `token` varchar(255) DEFAULT NULL,
  `serve_inst` varchar(255) DEFAULT NULL,
  `discount` float NOT NULL DEFAULT 0,
  `total_amnt` float NOT NULL DEFAULT 0,
  `Payment_Date` datetime NOT NULL,
  `tax` varchar(255) NOT NULL,
  `createdAt` datetime NOT NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`id`),
  KEY `customer_id` (`customer_id`),
  CONSTRAINT `orders_ibfk_1` FOREIGN KEY (`customer_id`) REFERENCES `customers` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `orders`
--

LOCK TABLES `orders` WRITE;
/*!40000 ALTER TABLE `orders` DISABLE KEYS */;
INSERT INTO `orders` VALUES (1,1,'0000-00-00 00:00:00','Ready','2','No green pepper',1.5,9.95,'0000-00-00 00:00:00','13%','2021-04-15 15:24:50'),(6,2,'2021-04-13 00:00:00','Ready','5','take-out',2.5,9.95,'2021-04-13 00:00:00','13%','2021-04-15 15:35:29'),(7,3,'2021-04-12 00:00:00','Not ready','2','Dining',5,12.95,'2021-04-12 00:00:00','13%','2021-04-15 15:35:29'),(8,4,'2021-04-10 00:00:00','In process','2','take_out',3,20.22,'2021-04-10 00:00:00','13%','2021-04-15 15:35:29'),(9,5,'2021-04-16 00:00:00','Ready','2','pick-up',2,50.15,'2021-04-15 00:00:00','13%','2021-04-15 15:35:29');
/*!40000 ALTER TABLE `orders` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-04-15 15:42:50
