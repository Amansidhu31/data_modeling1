DROP TABLE IF EXISTS menu;

CREATE TABLE menu (
menu_id INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
title VARCHAR(255),
type VARCHAR(255) NOT NULL ,
description text,
Veg enum ('Y', 'N'),
price DECIMAL(9,4) NOT NULL,
calories VARCHAR(255),
cooking TINYINT(1) NOT NULL DEFAULT 0,
image LONGBLOB Default null,
vendor TINYINT(1) NOT NULL DEFAULT 0,
Active BOOLEAN,
createdAt DATETIME NOT NULL,
updatedAt DATETIME NULL DEFAULT NULL
);

DROP TABLE IF EXISTS customers;

CREATE TABLE customers (
id INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
f_name VARCHAR(255),
l_name VARCHAR(255) NOT NULL ,
register_at DATETIME NOT NULL,
email VARCHAR(255),
cus_password FLOAT NOT NULL DEFAULT 0,
admin BOOLEAN,
country VARCHAR(255) NOT NULL,
city VARCHAR(255),
phone VARCHAR(255),
address VARCHAR(255) NOT NULL,
gender ENUM ('Femal', 'Male'),
createdAt DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
updatedAt DATETIME NULL DEFAULT NULL
);


DROP TABLE IF EXISTS orders;

CREATE TABLE orders (
id INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
customer_id INT NOT NULL,
order_datetime DATETIME NOT NULL,
order_status VARCHAR(255) NOT NULL ,
token VARCHAR(255),
serve_inst VARCHAR(255),
discount FLOAT NOT NULL DEFAULT 0,
total_amnt FLOAT NOT NULL DEFAULT 0,
Payment_Date DATETIME NOT NULL,
tax VARCHAR(255) NOT NULL,
createdAt DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
FOREIGN KEY (customer_id) REFERENCES customers(id)
);


DROP TABLE IF EXISTS Details;

CREATE TABLE Details (
order_id INT NOT NULL,
menu_id INT NOT NULL,
PRIMARY KEY (order_id, menu_id),
FOREIGN KEY (menu_id) REFERENCES menu(menu_id),
FOREIGN KEY (order_id) REFERENCES orders(id)
);

INSERT INTO menu 
( title, type, description, Veg, price, calories, cooking, image, vendor, Active)
VALUES
('Beef Dumplings', 'Lunch', 'Seasoned ground or minced beef, placed on bean curd skins and steamed', 'N', 10.95, 400, 1,'', 1, 1),
('Pawn Dumplings', 'Lunch', 'Dive into thses bite-sized dumplings with pawn filling', 'N', 8.95, 300, 1,'', 0, 1),
('Mixed-Veg Dumplings', 'Lunch', 'Served in the traditional bamboo steamer, these steamed dumplings are every veg-lover dream', 'Y', 8.95, 300, 0,'', 0, 1),
('Kurkra Dumplings', 'Lunch', 'Bite-size dumplings filled with perfect blend of kurkre and onions', 'Y', 9.95, 250, 1,'', 0, 1),
('Curried Cuttlefish', 'Lunch', 'Squid or cuttlefish is marinated in a curry sauce and steamed', 'N', 9.95, 250, 1,'', 0, 1),
('Veg-Bryani', 'Lunch', 'Veg-bryiani with delicious vegetables chunks and cheese', 'Y', 9.95, 350, 1,'', 1, 1),
('Veg-Noodles', 'Dinner', 'seasoned with oyster sauce, soy sauce, and sugar.', 'N', 9.95, 350, 1,'', 0, 1),
('Egg Custard', 'Desert', 'A steamed custard that may include meat or seafood', 'N', 5.95, 300, 1,'',1, 1),
('Mango Pudding', 'Desert', 'A light, refreshing dim sum dessert made with sweet fresh mango fruit.', 'N', 5.95, 300, 0,'', 1, 0),
('Sesame Seed Balls', 'Starters', 'Balls of glutinous rice flour and brown sugar are filled with sweet red bean paste, rolled in sesame seeds, and deep-fried', 'Y', 7.95, 300, 1,'', 0, 1),
('Shrimp Toast', 'Starter', 'A shrimp paste mixture is spread on toast points and deep-fried.', 'N', 7.95, 350, 1,'', 0, 1),
('Spring Rolls', 'Starter', 'A classic spring roll filling consists of dried mushrooms, and shredded meat, and carrot or bamboo shoots.', 'N', 8.95, 170, 0,'', 1, 1),
('Berrys Cheesecake', 'Desert', '6oz. and served with berrys ', 'Y', 5.95, 150, 1,'', 0, 1),
('Chocolate Brownie', 'Desert', 'Flavoured with Ice cream ', 'Y', 5.95, 300, 1,'', 0, 1),
('Sizzling White Chocolate Cake', 'Desert', '6oz. and served with berrys ', 'Y', 6.95, 200, 1,'', 0, 1),
('Stuffed Green Pepper', 'diner', 'Squares of green bell pepper are covered with shrimp paste and fried ', 'N', 6.95, 350, 1,'', 1,0),
('Stuffed Eggplant', 'Diner', 'Purple eggplant is stuffed with a shrimp paste fried and then topped with sauce.', 'N', 9.95, 300, 1,'', 0, 1),
('Roast Pork', 'Dinner', 'The filling used in roast pork buns', 'N',10.95, 400, 1,'', 1, 1),
('Chicken Pasta', 'Dinner', 'Pasta made in delicious Alferedo Sauce ', 'Y', 8.75, 300, 1,'', 0, 0);

INSERT INTO customers 
(f_name, l_name, register_at, email, cus_password, admin, country, city, phone, address, gender)
VALUES
('Dulea', 'Keirr', 2021-02-13, 'dk@gmail.com','234.5', 1, 'Canada', 'Winnipeg',2046089421,'810 chancellor','Male' ),
('Lockwood', 'Gary', 2021-01-15, 'lg@gmail.com','1234.1', 1, 'Canada', 'Winnipeg',2046884431,'944 pemibina hwy','Male'),
('Rain', 'Douglas', 2021-02-13, 'rg@hotmail.com','582.6', 0, 'Canada', 'Toronto',2046069421,'810 Guelph st','Female'),
('Fisher', 'Carrie',2021-02-10, 'fc@yahoo.com','686.6', 0, 'India', 'Faridkot',431658421,'810-944 pemibina hwy','Female'),
('Hammil', 'Mark', 2021-03-10, 'hammk@outlook.com','8012.3', 0, 'USA', 'new York',604897341,'1213 st.marry rd','Female');

INSERT INTO orders 
(customer_id, order_datetime, order_status, token, serve_inst, discount, total_amnt, Payment_Date, tax)
VALUES
(1,'2021-04-16', 'Ready', 2,'No green pepper', '1.5',9.95, '2021-04-15','13%'),
(2,'2021-04-13', 'Ready', 5,'take-out', '2.5',9.95, '2021-04-13','13%'),
(3,'2021-04-12', 'Not ready', 2,'Dining', '5',12.95, '2021-04-12','13%'),
(4,'2021-04-10', 'In process', 2,'take_out', '3',20.22, '2021-04-10','13%'),
(5,'2021-04-16', 'Ready', 2,'pick-up', '2',50.15, '2021-04-15','13%');

INSERT INTO Details
values
(1,1),
(6,2),
(7,3),
(8,4),
(9,5);
