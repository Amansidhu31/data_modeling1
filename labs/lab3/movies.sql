-- NOrmalized database
DROP TABLE IF EXISTS `movies`;

CREATE TABLE `movies` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) DEFAULT NULL,
  `year` int(11) DEFAULT NULL,
  `studio_id` int(11) DEFAULT NULL,
  `description` text DEFAULT NULL,
  `number_of_rentals` int(11) DEFAULT NULL,
  `created_at` DATETIME DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
);

INSERT INTO `movies` VALUES 
(1,'Black Panther',2018,1,'Black Panther elevates superhero cinema to thrilling new heights while telling one of the MCUs most absorbing stories and introducing some of its most fully realized characters',1,'2021-04-1210:10:10+05:30'),
(2,'Citizen Kane',1980,2,'Orson Welless epic tale of a publishing tycoons rise and fall is entertaining poignant, and inventive in its storytelling earning its reputation as a landmark achievement in film',2,'2021-04-1210:10:10+05:30'),
(3,'Parasite',2019,3,'An urgent brilliantly layered look at timely social themes Parasite finds writer-director Bong Joon Ho in near-total command of his craft',3,'2021-04-1210:10:10+05:30');
(4,'Avengers',2019,4,'Exciting, entertaining, and emotionally impactful, Avengers',4,'2021-04-1210:10:10+05:30');
(5,'Mission: Impossible',2019,5,'Fast, sleek, and fun, Mission: Impossible - Fallout',5,'2021-04-1210:10:10+05:30');

DROP TABLE IF EXISTS `studios`;

CREATE TABLE `studios` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(255) DEFAULT NULL,
  `country` VARCHAR(255) DEFAULT null,
  `created_at` int(11) default null,
  PRIMARY KEY (`id`)
);


INSERT INTO `studios` VALUES 
(1,'Theater of the Stars','USA','2021-04-1210:10:10+05:30' ),
(2,'Echo Lake','Canada','2021-04-1210:10:10:10+05:30'),
(3,'NBCUniversal','USA','2021-04-1210:10:10:10+05:30'),
(4,'Stephen King','USA','2021-04-1210:10:10:10+05:30'),
(5,'Star Wars: Galaxys Edg','USA','2021-04-1210:10:10:10+05:30');


DROP TABLE IF EXISTS `genre`;

CREATE TABLE `genre` (
  `genre_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`genre_id`)
);


INSERT INTO `genre` VALUES 
(1,'SF'),
(2,'Horror'),
(3,'Literature'),
(4,'Drama'),
(5,'Politics');


DROP TABLE IF EXISTS `genremovie`;

CREATE TABLE `genremovie` (
  `movie_id` int(11) NOT NULL AUTO_INCREMENT,
  `genre_id` int(11) NOT NULL,
  PRIMARY KEY (`movie_id`)
);


INSERT INTO `genremovie` VALUES 
(1,'1'),
(2,'2'),
(3,'2'),
(4,'4'),
(5,'5');


DROP TABLE IF EXISTS `actors`;

CREATE TABLE `actors` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `last_name` varchar(255) DEFAULT NULL,
  `given_name` varchar(255) DEFAULT NULL,
  `age` VARCHAR(255) DEFAULT NULL,
  `status` varchar(255) DEFAULT 'active',
  `created_at` TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
);

INSERT INTO `actors` VALUES 
(1,'Herbert','Ballantine','25','active','2021-04-1210:10:10+05:30'),
(2,'Ynez','Dell','35','inactive','2021-04-1210:10:10+05:30'),
(3,'King','Penguin','32','active','2021-04-1210:10:10+05:30'),
(4,'Laymon','Putnam','40','active','2021-04-1210:10:10+05:30'),
(5,'Twain','Delacorte','30','inactive','2021-04-1210:10:10+05:30');


DROP TABLE IF EXISTS `actormovie`;

CREATE TABLE `actormovie` (
  `movie_id` int(11) NOT NULL AUTO_INCREMENT,
  `actor_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`movie_id`)
);
INSERT INTO `actormovie` VALUES 
(1,'4'),
(2,'2'),
(3,'4'),
(4,'1'),
(5,'3');

DROP TABLE IF EXISTS `directors`;

CREATE TABLE `directors` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `last_name` varchar(255) DEFAULT NULL,
  `given_name` varchar(255) DEFAULT NULL,
  `age` int(11) DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
);
INSERT INTO `directors` VALUES 
(1,'Musa','Ballantine','30','active'),
(2,'Sidhu','Sukhjinder','35','inactive'),
(3,'William','Penguin','50','active'),
(4,'George','Tom','40','inactive'),
(5,'Twain','Delacorte','30','active');


DROP TABLE IF EXISTS `directormovie`;

CREATE TABLE `directormovie` (
  `movie_id` int(11) NOT NULL AUTO_INCREMENT,
  `director_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`movie_id`)
);
INSERT INTO `directormovie` VALUES 
(1,'4'),
(2,'2'),
(3,'4'),
(4,'1'),
(5,'3');
